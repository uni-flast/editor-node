class AttractorGroup {
    constructor() {
        this.attractors = [];
        this.distSquared = 1000;
    }
    activateAttractor (attractor) {
        this.attractors.push(attractor);
    }
    deactivateAttractor (attractor) {
        let i = this.attractors.indexOf(attractor);
        if(i >= 0) {
            this.attractors.splice(i,1);
        }
    }
    //simple distance check from elt's center
    //TODO: improve!
    //TODO: give each Attractor the possiblity to customize the collisionshape...
    checkIntersection (attractor1,attractor2){
        let rect1 = attractor1.getBoundingClientRect();
        let rect2 = attractor2.getBoundingClientRect();
        let dx = (rect1.left + rect1.width/2.0 - rect2.left - rect2.width/2.0);
        let dy = (rect1.top + rect1.height/2.0 - rect2.top - rect2.height/2.0);
        return dx*dx+dy*dy < this.distSquared;
    }

    //TODO: optimize pls!!!
    searchIntersects (elt){
        let intersects = [];
        let l = this.attractors.length;
        for( let i = 0; i < l; i++) {
            let attractor = this.attractors[i];
            if(attractor != elt) {
                if(this.checkIntersection(attractor,elt)) {
                    intersects.push(attractor);
                }
            }
        }
        return intersects;
    }
}


function Attractor(attractorGroups) {
    Attractor.constructor(this,attractorGroups);
}
/* allows easy inheritance but also component style behaviour*/
Attractor.makeAttractor = function(elt, attractorGroups) {
    Object.assign(elt,Attractor.prototype);
    Attractor.constructor(elt,attractorGroups);
}
Attractor.constructor = function(attractor,attractorGroups = []){
    attractor.attractorGroups = attractorGroups;
    attractor.activate();
    attractor.addEventListener("dragMove",attractor.scan);
    attractor.addEventListener("enterAttractorInfluence",attractor.enterInfluence);
    attractor.addEventListener("exitAttractorInfluence",attractor.exitInfluence);
}



//temporary
Attractor.prototype.enterInfluence = function(e) {
    if(e.detail.sender) {
        this.style.backgroundColor = "yellow";
    }
    else {
        this.style.backgroundColor = "purple";
    }
}
//temporary
Attractor.prototype.exitInfluence = function(e) {
    this.style.backgroundColor = null;
}
//default attractor selector, returns the first attractor found
//TODO: change to closest with persistence as long as previous still in range
//if an override exists it get called instead
Attractor.prototype.selectAttractorInfluence = function(attractors) {
    if(this.selectAttractorInfluenceOverride) {
        return this.selectAttractorInfluenceOverride(attractors);
    }
    else {
        return attractors[0];
    }
}



Attractor.prototype.scan = function() {
    let attractor = this.selectAttractorInfluence(this.searchIntersects());
    if(this.currentAttractor && attractor !== this.currentAttractor)
    {
        this.notifyExitAttractorInfluence(this.currentAttractor);
    }
    if(attractor && attractor !== this.currentAttractor) {
        this.notifyEnterAttractorInfluence(attractor);
    }
    this.currentAttractor = attractor;
}
Attractor.prototype.searchIntersects = function(){
    let intersects = [];
    this.attractorGroups.forEach(group => {
        intersects = intersects.concat(group.searchIntersects(this));
    });
    return intersects;
};
Attractor.prototype.notifyEnterAttractorInfluence = function(otherAttractor) {
    let evt1 = new CustomEvent("enterAttractorInfluence",{detail:{attractor:otherAttractor,sender:true}});
    this.dispatchEvent(evt1);
    let evt2 = new CustomEvent("enterAttractorInfluence",{detail:{attractor:this,sender:false}});
    otherAttractor.dispatchEvent(evt2);
};
Attractor.prototype.notifyExitAttractorInfluence = function(otherAttractor) {
    let evt1 = new CustomEvent("exitAttractorInfluence",{detail:{attractor:otherAttractor,sender:true}});
    this.dispatchEvent(evt1);
    let evt2 = new CustomEvent("exitAttractorInfluence",{detail:{attractor:this,sender:false}});
    otherAttractor.dispatchEvent(evt2);
};



/*helper functions */
Attractor.prototype.activate = function(){
    this.attractorGroups.forEach(group => {
        group.activateAttractor(this);
    });
    this.active = true;
};
Attractor.prototype.deactivate = function(){
    this.attractorGroups.forEach(group => {
        group.deactivateAttractor(this);
    });
    this.active = false;
};
Attractor.prototype.addAttractorGroup = function(attractorGroup) {
    this.attractorGroups.push(attractorGroup);
    if(this.active)
    {
        attractorGroup.activate(this)
    }
}
Attractor.prototype.removeAttractorGroup = function(attractorGroup) {
    this.attractorGroups.push(attractorGroup);
    if(this.active)
    {
        let i = this.attractorGroup.indexOf(attractor);
        if(i >= 0)
        {
            this.attractorGroups.splice(i,1);
        }
    }
}