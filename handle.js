//coordinates a group of handles
class HandleManager {
    constructor (editor) {
        this.editor = editor;
        this.nodeContainer = editor.nodeContainer;
        this.links = [];
        this.temporaryLinks = [];
        this.selectedHandles = [];
        //might want to do one for input and another for output
        this.attractorGroup = new AttractorGroup();

        this.floatingDiv = document.createElement("div");
        this.floatingDiv.classList.add("handle");
        Attractor.makeAttractor(this.floatingDiv,[this.attractorGroup]);

        let manager = this;
        this.floatingDiv.addEventListener("dragMove",function(){manager.dragMoveCallback();});
        this.floatingDiv.addEventListener("dragEnd", function(){
            if(this.currentAttractor)
            {
                let l = manager.selectedHandles.length;
                for(let i = 0; i < l; i++)
                {
                    manager.links.push({a:manager.selectedHandles[i].elt,b:this.currentAttractor});
                }
            }
            manager.nodeContainer.removeChild(manager.floatingDiv);
            manager.temporaryLinks = [];
            manager.editor.dirty = true;
        });
    }

    dragMoveCallback () {
        this.editor.dirty = true;
    }
}


class Handle {
    constructor(handleManager) {
        this.handleManager = handleManager;
        this.elt = document.createElement("div");
        this.elt.classList.add("handle");
        Attractor.makeAttractor(this.elt,[this.handleManager.attractorGroup]);
        let scope = this;
        this.elt.addEventListener("mousedown", function(e){scope.onmousedown(e);});
    }


    onmousedown (e) {
        e.stopPropagation();
        //HARDCODED
        if(!e.ctrlKey)
            this.handleManager.selectedHandles = [];
        this.handleManager.selectedHandles.push(this);
        this.handleManager.nodeContainer.appendChild(this.handleManager.floatingDiv);
        this.handleManager.temporaryLinks.push({a:this.elt,b:this.handleManager.floatingDiv});
        this.handleManager.floatingDiv.style.pointerEvents = "none";
        let rect = this.elt.getBoundingClientRect();
        let nodeContainerRect = this.handleManager.nodeContainer.getBoundingClientRect();
        let scaleX = (rect.width / this.elt.offsetWidth);
        let scaleY = (rect.height / this.elt.offsetHeight);
        this.handleManager.floatingDiv.style.left = (rect.left - nodeContainerRect.left) / scaleX + "px";
        this.handleManager.floatingDiv.style.top = (rect.top - nodeContainerRect.top) / scaleY + "px";
        Draggable.mousedown.call(this.handleManager.floatingDiv, e);
    }

}