class Draggable {
    constructor(element) {
        element.addEventListener("mousedown",Draggable.mousedown);
        element.style.position = "absolute";
    }

    //start drag
    static mousedown (e) {
        if(e.button == 0)
        {
            if(e.ctrlKey) console.log("ctrl");
            Draggable.element = this;
            let style = this.currentStyle || window.getComputedStyle(this);
            let parentRect = this.parentElement.getBoundingClientRect();

            let rect = Draggable.element.getBoundingClientRect();
            let scaleX = rect.width / Draggable.element.offsetWidth;
            let scaleY = rect.height / Draggable.element.offsetHeight;
            Draggable.diffX = parseInt(style.left.replace("px","")) * scaleX - e.clientX + parentRect.left;
            Draggable.diffY = parseInt(style.top.replace("px","")) * scaleY - e.clientY + parentRect.top;

            window.addEventListener("mousemove",Draggable.mousemove,true);
            window.addEventListener("mouseup",Draggable.mouseup);
            window.addEventListener("scroll",Draggable.calcPos,true);
            //scroll event doesn't get out of shadowDOM
            let root = this.getRootNode();
            if(root !== document) root.addEventListener("scroll",Draggable.calcPos,true);

            //required, else if click->move too soon, the browser tries to create a selection and the drag doesn't work
            e.preventDefault();
            this.dispatchEvent(Draggable.dragStartEvent);
        }
    }

    //drag is over, handle everything
    static mouseup (e) {
        if(e.button == 0)
        {
            window.removeEventListener("mousemove",Draggable.mousemove);
            window.removeEventListener("mouseup",Draggable.mouseup);
            window.removeEventListener("scroll",Draggable.calcPos,true);
            Draggable.element.getRootNode().removeEventListener("scroll",Draggable.calcPos,true);
            
            Draggable.element.dispatchEvent(Draggable.dragEndEvent);
            Draggable.element = null;
        }
    }

    //stores mousecoordinate and calls calcPos
    static mousemove (e) {
        Draggable.mouseX = e.clientX;
        Draggable.mouseY = e.clientY;
        Draggable.calcPos();
    }

    //calculates position and sets it
    static calcPos () {
        if(!Draggable.element) return;
        let rect = Draggable.element.getBoundingClientRect();
        let scaleX = rect.width / Draggable.element.offsetWidth;
        let scaleY = rect.height / Draggable.element.offsetHeight;
        let parentRect = Draggable.element.parentElement.getBoundingClientRect();
        Draggable.element.style.left = (Draggable.mouseX + Draggable.diffX - parentRect.left) / scaleX + "px";
        Draggable.element.style.top = (Draggable.mouseY + Draggable.diffY - parentRect.top) / scaleY + "px";

        Draggable.element.dispatchEvent(Draggable.dragMoveEvent);
    }

}

Draggable.dragStartEvent = new Event('dragStart');
Draggable.dragMoveEvent = new Event('dragMove');
Draggable.dragEndEvent = new Event('dragEnd');