class Node {
    constructor(editor) {
        this.editor = editor;
        this.elt = document.createElement("div");
        this.elt.classList.add("Node");
        let scope = this;
        this.elt.addEventListener("dragMove", function() {scope.dragMoveCallback();});
        new Draggable(this.elt);

    }
    dragMoveCallback () {
        this.editor.dirty = true;
    }

}