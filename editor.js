let PreferenceManager = {};

PreferenceManager.NodeEditor = {};
PreferenceManager.NodeEditor.zoom = "+";
PreferenceManager.NodeEditor.unzoom = "-";
PreferenceManager.NodeEditor.zoomFactor = 2;

class Editor {
    constructor (div) {
        /*let style = document.createElement("link");
        this.style = style;
        style.rel = "stylesheet";
        style.href = "Editors/node/style.css";
        style.type= "text/css";
        div.appendChild(style);*/

        let canvas = document.createElement("canvas");
        this.canvas = canvas;
        this.resize();
        canvas.id = "GraphCanvas";
        div.appendChild(canvas);

        window.addEventListener("resize",e => this.resize(e));

        let container = document.createElement("div");
        this.container = container;
        container.id = "container";
        div.appendChild(container);

        let root = document.createElement("div");
        this.root = root;
        root.id = "Root";
        container.appendChild(root);

        let nodeContainer = document.createElement("div");
        this.nodeContainer = nodeContainer;
        nodeContainer.id = "NodeContainer";
        root.appendChild(nodeContainer);

        this.ctx = this.canvas.getContext("2d");



        container.addEventListener("keydown", Editor.zoom,true);
        container.addEventListener("click", Editor.zoom,true);
        let scope = this;
        container.addEventListener("scroll",function(){scope.dirty = true;},false);
        window.addEventListener("keydown", function(e){scope.zoom(e);},true);



        this.handleManager = new HandleManager(this);
        this.attractorGroup = new AttractorGroup();

        
        //temp
        let node = new Node(this);
        node.elt.id = "Node1";
        nodeContainer.appendChild(node.elt);
        //Attractor.makeAttractor(node.elt,[this.attractorGroup]);

        let node2 = new Node(this);
        node2.elt.id = "Node2";
        nodeContainer.appendChild(node2.elt);
        //Attractor.makeAttractor(node2.elt,[this.attractorGroup]);
        /*node2.elt.addEventListener("dragEnd",function(){
            if(this.currentAttractor)
            {
                this.currentAttractor.style.backgroundColor = "purple";
                this.style.backgroundColor = "red";
                let style = window.getComputedStyle(this.currentAttractor);
                this.style.left = style.left;
                this.style.top = (parseInt(style.top.replace("px","")) + parseInt(style.height.replace("px",""))) + "px";
                scope.dirty = true;
            }
        });*/

        let handle = new Handle(this.handleManager);
        this.nodeContainer.appendChild(handle.elt);
        node.elt.appendChild(handle.elt);

        let handle2 = new Handle(this.handleManager);
        this.nodeContainer.appendChild(handle2.elt);
        node2.elt.appendChild(handle2.elt);



        this.scaleFactor = 1;
        this.dirty = true;
        
        this.renderLoop();
    }

    resize() {
        this.canvas.setAttribute("width",document.body.clientWidth-1);
        this.canvas.width = document.body.clientWidth-1;
        this.canvas.setAttribute("height",document.body.clientHeight-1);
        this.canvas.height = document.body.clientHeight-1;
        this.dirty = true;
    }
    
    zoom(e) {
        console.log(e);
        if(e.key === PreferenceManager.NodeEditor.zoom) {
            this.scale(PreferenceManager.NodeEditor.zoomFactor);
        }
        else if(e.key === PreferenceManager.NodeEditor.unzoom) {
            this.scale(1/PreferenceManager.NodeEditor.zoomFactor);
        }
    }
    scale(factor) {
        this.scaleFactor *= factor;
        this.root.style.transform = "scale(" + this.scaleFactor + "," + this.scaleFactor + ")";
        this.dirty = true;
        Draggable.calcPos();
    }
    renderLoop(){
        if(this.dirty)
        {
            this.dirty = false;
            let ctx = this.ctx;
            let nodeContainer = this.nodeContainer;
            let scaleFactor = this.scaleFactor;
            let container = this.container;
            let canvas = this.canvas;

            ctx.setTransform(1, 0, 0, 1, 0, 0);
            ctx.clearRect(0,0,canvas.width, canvas.height);
            let style = nodeContainer.currentStyle || window.getComputedStyle(nodeContainer);
            let left = parseInt(style.left.replace('px',''));
            let top = parseInt(style.top.replace('px',''));
            ctx.translate(-container.scrollLeft + nodeContainer.offsetLeft * scaleFactor,- container.scrollTop + nodeContainer.offsetTop* scaleFactor);
            ctx.scale(scaleFactor,scaleFactor);

            this.handleManager.links.forEach(link => {Editor.drawLinks(link,nodeContainer,ctx,scaleFactor,"rgb(0,100,0)",3)});
            this.handleManager.temporaryLinks.forEach(link => {Editor.drawLinks(link,nodeContainer,ctx,scaleFactor,"rgb(0,200,0)")});
        }
        let scope = this;
        requestAnimationFrame(function(){scope.renderLoop();});
    }
    static drawLinks (link,container,ctx,scaleFactor,strokeStyle,lineWidth = 5) {
        let a = link.a;
        let b = link.b;
        let containerRect = container.getBoundingClientRect();
        let aRect = a.getBoundingClientRect();
        let bRect = b.getBoundingClientRect();
        
        let x1 = (aRect.left - containerRect.left + aRect.width/2)/scaleFactor;
        let y1 = (aRect.top - containerRect.top + aRect.height/2)/scaleFactor;
        let x2 = (bRect.left - containerRect.left + bRect.width/2)/scaleFactor;
        let y2 = (bRect.top - containerRect.top + bRect.height/2)/scaleFactor;
        let dist = x2-x1;
        ctx.beginPath();
        
        ctx.moveTo(x1,y1);
        //might want to customize middle control points
        ctx.bezierCurveTo(x1+dist*0.8,y1,x2-dist*0.8,y2,x2,y2);
        ctx.lineWidth = lineWidth;

        ctx.strokeStyle = strokeStyle;
        ctx.stroke();
    }
}

new Editor(document.getElementById("editor"));
